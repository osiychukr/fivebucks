//
//  ContactsViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 20.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class ContactsViewController: UIViewController {
    @IBOutlet weak var contactTextView: UITextView!
    override func viewDidLoad() {
        let string1 = "базой знаний"
        let string = "Уважаемые пользователи! \n\nРады приветствовать Вас на нашем проекте 5bucks.ru \n\nПредлагаем Вам ознакомиться с нашей базой знаний, где вы найдете ответы на часто задаваемые вопросы. \n\nНаш e-mail для прямого обращения к нам: support@5bucks.ru"
        let linkString = NSMutableAttributedString(string: string)
        linkString.addAttribute(NSAttributedStringKey.link, value: NSURL(string: "http://support.5bucks.ru/")!, range: NSMakeRange(115, string1.characters.count))
        linkString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "RobotoCondensed-Regular", size: 14.0)!, range: NSMakeRange(0, string.characters.count))
        contactTextView.attributedText = linkString
        
    }
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

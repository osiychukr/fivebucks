//
//  RulesViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 20.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class RulesViewController: UIViewController {
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeight.constant = UIScreen.main.bounds.height-85
    }
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

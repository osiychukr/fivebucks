//
//  FilterViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 17.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class FilterViewController: UIViewController {
 
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var newFirstButton: UIButton!
    @IBOutlet weak var oneDayButton: UIButton!
    @IBOutlet weak var threeDayButton: UIButton!
    @IBOutlet weak var sevenDayButton: UIButton!
    @IBOutlet weak var defaultDayButton: UIButton!
    @IBOutlet weak var showOnlineSellersButton: UIButton!
    @IBOutlet weak var applyFiltersButton: UIButton!
    
    
    var order = orderType.def
    var day_avg = day_avgType.def
    var online_sellers = onlineSellersType.def
    var isOrderNew = false

    var isOnlineSellers = false
    var activeDayButton = UIButton()
    
    var delegate: FilterResultsProtocol?
    
    // MARK: - Main
    override func viewDidLoad() {
        setStartState()
    }
    
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ratingButtonTapped(_ sender: Any) {
        if isOrderNew {
            ratingButton.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            order = orderType.def
            newFirstButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            isOrderNew = !isOrderNew
        }
    }
    
    @IBAction func newFirstButtonTapped(_ sender: Any) {
        if !isOrderNew {
            newFirstButton.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            order = orderType.new
            ratingButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            isOrderNew = !isOrderNew
        }
    }
    
    @IBAction func oneDayButtonTapped(_ sender: UIButton) {
        if !sender.isEqual(activeDayButton) {
            sender.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            day_avg = day_avgType.one
            activeDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            activeDayButton = sender
        }
    }
    @IBAction func threeDayButtonTapped(_ sender: UIButton) {
        if !sender.isEqual(activeDayButton) {
            sender.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            day_avg = day_avgType.three
            activeDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            activeDayButton = sender
        }
    }
    @IBAction func sevenDayButtonTapped(_ sender: UIButton) {
        if !sender.isEqual(activeDayButton) {
            sender.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            day_avg = day_avgType.seven
            activeDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            activeDayButton = sender
        }
    }
    @IBAction func defaultDayButtonTapped(_ sender: UIButton) {
        if !sender.isEqual(activeDayButton) {
            sender.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            day_avg = day_avgType.def
            activeDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            activeDayButton = sender
        }
    }
    
    @IBAction func showOnlineSellersButtonTapped(_ sender: Any) {
        if !isOnlineSellers {
            showOnlineSellersButton.setImage(#imageLiteral(resourceName: "filterCheck on"), for: .normal)
            online_sellers = onlineSellersType.online
        } else {
            showOnlineSellersButton.setImage(#imageLiteral(resourceName: "filterCheck off"), for: .normal)
            online_sellers = onlineSellersType.def
        }
        isOnlineSellers = !isOnlineSellers
    }
    
    @IBAction func applyFilterButtonsTapped(_ sender: Any) {
        self.delegate?.filterResults(day_avgFilter: day_avg, orderFilter: order, onlineSellersFilter: online_sellers)
        self.dismiss(animated: true, completion: nil)
    }
    func setStartState() -> Void {
        if order == orderType.def {
            ratingButton.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            newFirstButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            isOrderNew = false
        } else {
            ratingButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            newFirstButton.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            isOrderNew = true
        }
        if day_avg == day_avgType.def {
            defaultDayButton.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            oneDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            threeDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            sevenDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            activeDayButton = defaultDayButton
        } else if day_avg == day_avgType.one {
            defaultDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            oneDayButton.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            threeDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            sevenDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            activeDayButton = oneDayButton
        } else if day_avg == day_avgType.three {
            defaultDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            oneDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            threeDayButton.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            sevenDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            activeDayButton = threeDayButton
        } else {
            defaultDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            oneDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            threeDayButton.setImage(#imageLiteral(resourceName: "filterRadio off"), for: .normal)
            sevenDayButton.setImage(#imageLiteral(resourceName: "filterRadio on"), for: .normal)
            activeDayButton = sevenDayButton
        }
        
        if online_sellers == onlineSellersType.def {
            showOnlineSellersButton.setImage(#imageLiteral(resourceName: "filterCheck off"), for: .normal)
            isOnlineSellers = false
        } else {
            showOnlineSellersButton.setImage(#imageLiteral(resourceName: "filterCheck on"), for: .normal)
            isOnlineSellers = true
        }
    }
}

//
//  CategoriesTableViewCell.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 09.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class CategoriesTableViewCell: UITableViewCell {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryHeaderLabel: UILabel!
    
    func loadImageFromInternet(urlString: String) -> Void { ReciveData.loadImageFromInternetTableViewCell(urlString: urlString, imageView: self.categoryImage)
        
    }
}

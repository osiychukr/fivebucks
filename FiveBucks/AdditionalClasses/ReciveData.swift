//
//  ReciveData.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 06.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import TTGSnackbar


class ReciveData {
    
    class func getData (dataType: ApiModel, completion: @escaping (_ data: SwiftyJSON.JSON) -> Void)
    {
        
        Alamofire.request(dataType).responseJSON { response in
            
            guard response.result.isSuccess else {
                print("Error with network: \(String(describing: response.result.error))")
                let snackbar = TTGSnackbar(message: "Ошибка подключения к интернету!", duration: .short)
                snackbar.show()
                return
            }
            
            if  (response.result.value as? Dictionary <String, AnyObject>) != nil{
                ///!!!!!!!
                let json = SwiftyJSON.JSON( response.data!)
                print("JSON: \(json)")
                completion(json)
                
            } else {
                print("Invalid data information received from the service")
                return
            }
        }
}
    class func postData (url: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil, completion: @escaping (_ data: SwiftyJSON.JSON) -> Void)
    {
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseJSON { response in
            
            guard response.result.isSuccess else {
                print("Error with network: \(String(describing: response.result.error))")
                let snackbar = TTGSnackbar(message: "Ошибка подключения к интернету!", duration: .short)
                snackbar.show()
                return
            }
            
            if  (response.result.value as? Dictionary <String, AnyObject>) != nil{
                ///!!!!!!!
                let json = SwiftyJSON.JSON( response.data!)
                print("JSON: \(json)")
                if json["code"].stringValue == "401" {
                    GlobalFunctions.logIn()
                    completion(json)
                } else {
                    completion(json)
                }
            } else {
                print("Invalid data information received from the service")
                return
            }
        }
    }
    // FOR DOWNLOADING IMAGES
    class func documentsDirectoryUrl() -> URL {
        let documentsUrl = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) //FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).last!
        return documentsUrl
        
    }
    class func saveImageFileToPath(imageData: Data,imageFileName: String) -> Void {
        
        
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).last!
        let filePath = documentsURL.path
        //        print("11111 \(filePath)")
        let fileName = String(format: "%@/%@", filePath, imageFileName)
        //        print(fileName)
        let newImage = UIImage(data:imageData,scale:1.0)
        let path = URL(fileURLWithPath: fileName)
        //        print(path)
        if newImage != nil {
            do {
                try UIImagePNGRepresentation(newImage!)!.write(to: path)
                print("Image Added Successfully")
            } catch {
                print(error)
            }} else {
            print("Wrong image data")
        }
    }
    
    class func loadImageFromInternetTableViewCell(urlString: String, imageView: UIImageView) -> Void {
        
//        let currentCellNumber = selfCell.currentCellNumber
        imageView.image = nil
        loadImageFromInternet(urlString: urlString, completion: { imageData in
//            if (currentCellNumber == selfCell.currentCellNumber) {
                DispatchQueue.main.async( execute: {
                    imageView.image = UIImage(data:imageData as Data)
//                    selfCell.serviceImageView.image = UIImage(data:imageData as Data)
//                    selfCell.serviceImageView.layer.masksToBounds = true
//                    selfCell.serviceImageView.layer.cornerRadius = 3
                })
//            }
        })
    }
    
    class func loadImageFromInternet(urlString: String, completion: @escaping (_ imageData: NSData) -> Void) {
        
        if urlString != ""{
            
            DispatchQueue(label: "loadingQueue", attributes: .concurrent).async {

                let url = URL(string: urlString)
                let fileName = url?.pathComponents.last
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask).last!
                let fullFileName = String(format: "%@/%@", documentsURL.path, fileName!)
                var imageData: NSData? = nil;
                if FileManager.default.fileExists(atPath: fullFileName){
                    imageData = NSData(contentsOfFile: fullFileName)
                } else {
                    
                    do {
                        let orderData = try NSData(contentsOf: url!, options: NSData.ReadingOptions.dataReadingMapped)
                        imageData = orderData
                        self.saveImageFileToPath(imageData: imageData! as Data, imageFileName: fileName!)
                    } catch  {
                        print("Could not load data \(error.localizedDescription)")
                        imageData = (UIImagePNGRepresentation(#imageLiteral(resourceName: "placeholderImage")) as NSData?)
                    }
                }
                
                completion(imageData!)
                
            }
        }
    }
}

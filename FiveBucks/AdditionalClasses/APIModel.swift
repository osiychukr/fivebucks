//
//  APIModel.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 06.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//
import Foundation
import Alamofire

public enum ApiModel: URLRequestConvertible {
    static let baseURLPath = "https://5bucks.ru/openapi?api="

//    static let authenticationToken = ""
    
    // user
    case register(String, String, String, String, String, String)
    // jobs
    case get_home_jobs
    case get_categories
    case get_subcategories(String)
    case autocomplete(String)
    case get_category_jobs(String, Int, Int)
    case search(String?, Int, Int, String?, String?, String?, String?)
    case single_job(String)
    case single_job_adds(String)
    case single_job_revs(String, Int, Int)
    // request
    
    var method: HTTPMethod {
        switch self {
        case .register:
            return .get
        case .get_home_jobs, .get_subcategories, .get_categories, .autocomplete, .get_category_jobs, .search, .single_job, .single_job_adds, .single_job_revs:
            return .get
        default:
            return .post
        }
    }
    
    var path: String {
        switch self {
//      users
        case .register:
            return "users.register"
// jobs
        case .autocomplete:
            return "jobs.autocomplete"
        case .get_home_jobs:
            return "jobs.get_home_jobs"
        case .get_categories:
            return "jobs.get_categories"
        case .get_subcategories:
            return "jobs.get_subcategories"
        case .get_category_jobs:
            return "jobs.get_category_jobs"
        case .search:
            return "jobs.search"
        case .single_job:
            return "jobs.single_job"
        case .single_job_adds:
            return "jobs.single_job_adds"
        case .single_job_revs:
        return "jobs.single_job_revs"
        }
    }
    
    public func asURLRequest() throws -> URLRequest {
        let parameters: [String: Any] = {
            switch self {
            // user
            case .register(let provider, let login, let email, let password, let identifier, let profileURL):
                return ["provider": provider, "data[login]": login, "data[email]": email, "data[password]": password, "provider_data[identifier]": identifier, "provider_data[profileURL]": profileURL]
            // jobs
            case .get_home_jobs, .get_categories:
                return [:]
            case .get_subcategories(let parent):
                return ["data[parent]": parent]
            case .autocomplete(let search):
                return ["data[search]": search]
            case .get_category_jobs(let category, let skip, let take):
                return ["data[category]": category, "data[skip]": skip, "data[take]": take]
            case .search(let search, let skip, let take, let category, let order, let day_avg, let online_sellers):
                return ["data[search]": search ?? "", "data[skip]": skip, "data[take]": take, "data[category]": category ?? "", "data[order]": order ?? "", "data[day_avg]": day_avg ?? "", "data[online_sellers]": online_sellers ?? ""]
            case .single_job(let jobid), .single_job_adds(let jobid):
                return ["data[jobid]": jobid]
            case .single_job_revs(let jobid, let skip, let take):
                return ["data[jobid]": jobid, "data[skip]": skip, "data[take]": take]
            default:
                return [:]
            }
        }()
        
        let url = try (ApiModel.baseURLPath+path).asURL()
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        //        request.setValue(ApiModel.authenticationToken, forHTTPHeaderField: "Authorization")
        request.timeoutInterval = TimeInterval(10 * 1000)
        return try URLEncoding.default.encode(request, with: parameters)
    }
}

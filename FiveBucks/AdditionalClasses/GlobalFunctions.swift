//
//  GlobalFunctions.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 06.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class GlobalFunctions {
    static public func setShadow(object: UIView, radius: CGFloat? = 2.0, opacity: Float? = 0.3, offsetWidth: CGFloat? = 0.0, offsetHeifght: CGFloat? = 2.0) -> Void {
        object.layer.shadowColor = UIColor.black.cgColor
        object.layer.shadowRadius  = radius!
        object.layer.shadowOpacity = opacity!
        object.layer.shadowOffset  = CGSize(width: offsetWidth!, height: offsetHeifght!)
    }
    
    static public func setOnlineStatus(object: UIImageView, status:String) -> Void {
        if status == "0" {
            object.image = #imageLiteral(resourceName: "mainOffline")
        } else if status == "1" {
            object.image = #imageLiteral(resourceName: "mainOnline")
        }
    }
    static public func setFavoriteStatus(object: UIButton, status:String) -> Void {
        if status == "0" || status == "false" {
            object.setImage(#imageLiteral(resourceName: "mainFavorite-heart-button"), for: .normal)
        } else if status == "1" || status == "true"  {
            object.setImage(#imageLiteral(resourceName: "mainFavorite-heart-buttonFull"), for: .normal)
  
        }
    }

    static public func setFavoriteStatusOnTop(object: UIButton, status:String) -> Void {
        if status == "0" || status == "false" {
            object.setImage(#imageLiteral(resourceName: "jobFavorite"), for: .normal)
        } else if status == "1" || status == "true"  {
            object.setImage(#imageLiteral(resourceName: "mainFavorite-heart-button"), for: .normal)
            
        }
    }
    
    static public func setAuthorLevel(object: UIImageView, label:UILabel, level:String) -> Void {
        if level == "0" {
            object.image = nil
            label.text = ""
        } else if level == "1" {
            object.image = #imageLiteral(resourceName: "mainRocket")
            label.text = "АКТИВНЫЙ ПРОДАВЕЦ"
        } else if level == "2" {
            object.image = #imageLiteral(resourceName: "mainStar")
            label.text = "ТОП-ПРОДАВЕЦ"
        } else if level == "3" {
            object.image = #imageLiteral(resourceName: "mainCup")
            label.text = "5BUCKS ЛИДЕР"
        }
    }
    static public func setCurency(curency: String) -> String {
        if curency == "RUB" {
            return "\u{20BD}"
        }
        return "\u{0024}"
    }
    static public func alertWithTitle(title: String!, message: String, ViewController: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: {_ in
        });
        alert.addAction(action)
        ViewController.present(alert, animated: true, completion:nil)
    }
    
    static public func downloadAutocompleeteResults(search: String, completion: @escaping (_ results: [SearchTextFieldItem]) -> Void){
        ReciveData.getData(dataType: ApiModel.autocomplete(search), completion: { data in
            if Int(data["res"]["code"].stringValue)! > 0 {
                var resuts = [SearchTextFieldItem]()
                let autocomplete = data["autocomplete"].arrayValue
                let count = autocomplete.count
                for i in stride(from: 0, to: count, by: 1)  {
                    resuts.append(SearchTextFieldItem(title: autocomplete[i].stringValue))
                }
                completion(resuts)
            } else if Int(data["res"]["code"].stringValue)! < 0 {
                print (data["res"]["msg"].stringValue)
                completion([SearchTextFieldItem]())
            }
            
        })
    }
    static public func isValidEmail (test:String) ->Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: test)
    }
    
}
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

//
//  SignInViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 23.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class SignInViewController: UIViewController {
    @IBOutlet weak var googleButton: UIButton!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var vkButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func googleButtonTapped(_ sender: Any) {
    }
    @IBAction func facebookButtonTapped(_ sender: Any) {
    }
    @IBAction func vkButtonTapped(_ sender: Any) {
    }
    @IBAction func emailButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "ShowEmailSignVC", sender: nil)
    }
}

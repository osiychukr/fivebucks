//
//   ForgotPasswordViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 23.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailTextView: UITextField!
    @IBOutlet weak var emailUnderlineView: UIView!
    
    @IBOutlet weak var enterButton: UIButton!
    
    
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func enterButtonTapped(_ sender: Any) {
    }
    
}

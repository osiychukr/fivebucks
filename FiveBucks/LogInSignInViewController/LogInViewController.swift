//
//  LogInViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 23.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class LogInViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextView: UITextField!
    @IBOutlet weak var emailUnderlineView: UIView!
   
    @IBOutlet weak var passwordTextView: UITextField!
    @IBOutlet weak var passwordUnderlineView: UIView!
    
    @IBOutlet weak var enterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterButton.layer.cornerRadius = 5
        GlobalFunctions.setShadow(object: enterButton)
    }
    
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func forgetPasswordButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "ShowForgotPasswordVC", sender: nil)
    }
    
    @IBAction func enterButtonTapped(_ sender: Any) {
    }
    @IBAction func logInGoogleButtonTapped(_ sender: Any) {
    }
    @IBAction func loginVKButtonTapped(_ sender: Any) {
    }
    @IBAction func loginFacebookButtonTapped(_ sender: Any) {
    }
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "ShowSignVC", sender: nil)
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(emailTextView) {
            emailUnderlineView.backgroundColor = UIColor(hex: "6FAE38")
        } else if textField.isEqual(passwordTextView) {
            passwordUnderlineView.backgroundColor = UIColor(hex: "6FAE38")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.isEqual(emailTextView) {
            emailUnderlineView.backgroundColor = UIColor(hex: "D3D3D3")
        } else if textField.isEqual(passwordTextView) {
            passwordUnderlineView.backgroundColor = UIColor(hex: "D3D3D3")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(emailTextView) {
            passwordTextView.becomeFirstResponder()
        } else if textField.isEqual(passwordTextView) {
            textField.resignFirstResponder()
        }
        return true
    }
    
    // MARK: - Additionals
    
    func checkForErrors() -> Bool
    {
        var errors = false
        let title = "Помилка"
        var message = ""
        
        if (emailTextView.text?.isEmpty)!
        {
            errors = true
            message += "Не заповнена електронна пошта користувача"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)
            
            //            self.emailLabel.becomeFirstResponder()
        }
        else if !GlobalFunctions.isValidEmail(test: emailTextView.text!)
        {
            errors = true
            message += "Невірний формат електронної пошти"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)
        }
        else if (passwordTextView.text?.isEmpty)!
        {
            errors = true
            message += "Не заповнений пароль"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)
        }
        else if (passwordTextView.text?.characters.count)!<6
        {
            errors = true
            message += "Пароль менше 6 символів"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)
        }
        
        return errors
    }
}

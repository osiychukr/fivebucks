//
//  EmailSignInViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 23.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit

struct Providers {
    static let  base = "base"
    static let  Vkontakte = "Vkontakte"
    static let  Facebook = "Facebook"
    static let  Google = "Google"
}

class EmailSignInViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var userNameTextView: UITextField!
    @IBOutlet weak var userNameUnderlineView: UIView!
    
    @IBOutlet weak var emailTextView: UITextField!
    @IBOutlet weak var emailUnderlineView: UIView!
    
    @IBOutlet weak var passwordTextView: UITextField!
    @IBOutlet weak var passwordUnderlineView: UIView!
    
    
    @IBOutlet weak var enterButton: UIButton!
    
    var dict : [String : AnyObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterButton.layer.cornerRadius = 5
        GlobalFunctions.setShadow(object: enterButton)
    }
    
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func enterButtonTapped(_ sender: Any) {
        if !checkForErrors() {
            
        }
    }
    @IBAction func logInGoogleButtonTapped(_ sender: Any) {
    }
    @IBAction func loginVKButtonTapped(_ sender: Any) {
    }
    @IBAction func loginFacebookButtonTapped(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile, .email ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                self.getFBUserData()
            }
        }
    }
    // MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(userNameTextView) {
            userNameUnderlineView.backgroundColor = UIColor(hex: "6FAE38")
        } else if textField.isEqual(emailTextView) {
            emailUnderlineView.backgroundColor = UIColor(hex: "6FAE38")
        } else if textField.isEqual(passwordTextView) {
            passwordUnderlineView.backgroundColor = UIColor(hex: "6FAE38")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.isEqual(userNameTextView) {
            userNameUnderlineView.backgroundColor = UIColor(hex: "D3D3D3")
        } else if textField.isEqual(emailTextView) {
            emailUnderlineView.backgroundColor = UIColor(hex: "D3D3D3")
        } else if textField.isEqual(passwordTextView) {
            passwordUnderlineView.backgroundColor = UIColor(hex: "D3D3D3")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(userNameTextView) {
            emailTextView.becomeFirstResponder()
        } else if textField.isEqual(emailTextView) {
            passwordTextView.becomeFirstResponder()
        } else if textField.isEqual(passwordTextView) {
            textField.resignFirstResponder()
        }
        return true
    }
    
    // MARK: - Additionals
    func showLoginView() -> Bool {
        
        return true
    }
    func checkForErrors() -> Bool {
        var errors = false
        let title = "Ошибка"
        var message = ""
        
        if (userNameTextView.text?.isEmpty)!
        {
            errors = true
            message += "Имя пользователя не должно быть пустым"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)
        }
        else if (userNameTextView.text?.characters.count)!<4
        {
            errors = true
            message += "Имя пользователя должно быть длиннее 4 символов"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)
        }
        else if (emailTextView.text?.isEmpty)!
        {
            errors = true
            message += "Адрес электронной почты не должен быть пустым"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)

        }
        else if !GlobalFunctions.isValidEmail(test: emailTextView.text!)
        {
            errors = true
            message += "Неверный адрес электронной почты"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)
        }
        else if (passwordTextView.text?.isEmpty)!
        {
            errors = true
            message += "Пароль не должен быть пустым"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)
        }
        else if (passwordTextView.text?.characters.count)!<6
        {
            errors = true
            message += "Пароль должен быть длиннее 6 символов"
            GlobalFunctions.alertWithTitle(title: title, message: message, ViewController: self)
        }
        
        return errors
    }

    //function is fetching the user data
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, first_name, last_name, gender,birthday"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                }
            })
        }
    }
    
    func sendRegistrationData(provider: String, login: String, email: String, password: String, identifier: String, profileURL: String) -> Void {
        ReciveData.getData(dataType: ApiModel.register(provider, login, email, password, identifier, profileURL), completion: { data in
            if Int(data["res"]["code"].stringValue)! > 0 {
                let token = data["auth_token"].stringValue

            } else if Int(data["res"]["code"].stringValue)! < 0 {
                print (data["res"]["msg"].stringValue)
                GlobalFunctions.alertWithTitle(title: "Помилка", message: data["res"]["msg"].stringValue, ViewController: self)
            }
            
        })
    }
}

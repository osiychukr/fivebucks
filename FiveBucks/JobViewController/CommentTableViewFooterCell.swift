//
//  CommentTableViewFooterCell.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 11.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class CommentTableViewFooterCell: UITableViewCell {
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var authorReplyBackgroundView: UIView!
    @IBOutlet weak var authorReplyLabel: UILabel!
    
}

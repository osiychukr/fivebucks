//
//  CommentTableViewCell.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 11.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var commentBeckgroundView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var commentTextLabel: UILabel!
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var commentLikeImage: UIImageView!
    @IBOutlet weak var commentRatingCosmosView: CosmosView!
    @IBOutlet weak var commentDateLabel: UILabel!
    @IBOutlet weak var commentTimeLabel: UILabel!
    
    func loadImageFromInternet(urlString: String) -> Void { ReciveData.loadImageFromInternetTableViewCell(urlString: urlString, imageView: self.userImageView)
        
    }
}

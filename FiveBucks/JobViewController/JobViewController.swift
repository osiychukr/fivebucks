//
//  JobViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 11.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
struct JobFullInfo {
    var singleJob = SingleJob()
    var singleJobAdds = SingleJobAdds()
    var jobReviews = [JobReview]()
}
struct SingleJob {
    var id: String = ""
    var title: String = ""
    var description: String = ""
    var author_id: String = ""
    var author: String = ""
    var author_level: String = ""
    var author_online: String = ""
    var top: String = ""
    var rating_count: String = ""
    var rating: String = ""
    var c1: String = ""
    var c2: String = ""
    var c3: String = ""
    var c4: String = ""
    var c5: String = ""
    var c_show: String = ""
    var rec_no: String = ""
    var rec_yes: String = ""
    var images = [String]()
    var author_avatar: String = ""
    var is_fav: String = ""
}

struct SingleJobAdds {
    var avg_order_done: String = ""
    var verified_email: String = ""
    var author_id: String = ""
    var author_info: String = ""
    var completed: String = ""
    var queue: String = ""
}

struct JobReview {
    var id: String = ""
    var rating: String = ""
    var review: String = ""
    var date: String = ""
    var comm: String = ""
    var rec: String = ""
    var author_id: String = ""
    var author: String = ""
    var answer: String = ""
    var author_avatar: String = ""
}

class JobViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    
    @IBOutlet weak var mainScrollView: UIScrollView!
//    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var numberOfJobImageLabel: UILabel!
    
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var authorOnlineStatusImageView: UIImageView!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var authorStatusImage: UIImageView!
    @IBOutlet weak var writeToAuthorButton: UIButton!
    @IBOutlet weak var openAuthorDetailsButton: UIButton!
    
    @IBOutlet weak var authorDetailsView: UIView!
    @IBOutlet weak var aboutAuthorLabel: UILabel!
    @IBOutlet weak var authorWorkTimeLabel
    : UILabel!
    @IBOutlet weak var authorEmailApprovedLabel: UILabel!
    @IBOutlet weak var authorOrdersDoneLabel: UILabel!
    @IBOutlet weak var authorOrdersInQueueLabel: UILabel!
    
    @IBOutlet weak var jobDetailsView: UIView!
    @IBOutlet weak var jobHeaderLabel: UILabel!
    @IBOutlet weak var infoAboutJobLabel: UILabel!
    
    @IBOutlet weak var orderJobView: UIView!
    @IBOutlet weak var orderJobButton: UIButton!
    @IBOutlet weak var jobCommetsCountLabel: UILabel!
    @IBOutlet weak var ratingJobCosmosView: CosmosView!
    @IBOutlet weak var ratingJobLabel: UILabel!
    @IBOutlet weak var jobLikesLabel: UILabel!
    @IBOutlet weak var jobEmojiLabel: UILabel!
    @IBOutlet weak var jobLikesImageView: UIImageView!
    @IBOutlet weak var jobEmojiImageView: UIImageView!
    
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var commentsTabeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var authorAddsViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emojiView: UIView!
    @IBOutlet weak var jobC1Image: UIImageView!
    @IBOutlet weak var jobC2Image: UIImageView!
    @IBOutlet weak var jobC3Image: UIImageView!
    @IBOutlet weak var jobC4Image: UIImageView!
    @IBOutlet weak var jobC5Image: UIImageView!
    @IBOutlet weak var jobC1Label: UILabel!
    @IBOutlet weak var jobC2Label: UILabel!
    @IBOutlet weak var jobC3Label: UILabel!
    @IBOutlet weak var jobC4Label: UILabel!
    @IBOutlet weak var jobC5Label: UILabel!
    
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var unlikeImage: UIImageView!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var unlikeLabel: UILabel!
    
    let defaults = UserDefaults.standard
//    var jobId = ""
    var jobFullInfo = JobFullInfo()
    var singleJob = SingleJob()
    var singleJobAdds = SingleJobAdds()
    var jobReviews = [JobReview]()
    
    var maxLoadedValue = 0
    var maxExistedValue = 0
    
    var job = Jobs()
    
    var authorAddsIsHidden = true
    var likeViewIsHidden = true
    var emojiViewIsHidden = true
    var descriptionTextIsHidden = true
    
    @IBOutlet var pageControl: UIPageControl?
    @IBOutlet var scrollView: UIScrollView?
    var scrollWidth : CGFloat = UIScreen.main.bounds.size.width
    var scrollHeight : CGFloat = 183
    
    @IBAction func changePage(){
        
        scrollView!.scrollRectToVisible(CGRect(x: scrollWidth * CGFloat ((pageControl?.currentPage)!), y: 0, width: scrollWidth, height: scrollHeight), animated: true)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setIndiactorForCurrentPage()
    }
    
    func setIndiactorForCurrentPage()  {
        let page = (scrollView?.contentOffset.x)!/scrollWidth
        pageControl?.currentPage = Int(page)
        numberOfJobImageLabel.text = "\(Int(page)+1)/\(singleJob.images.count)"
        
    }
    //MARK: - Main
    override func viewDidLoad() {
        // Do any additional setup after loading the view, typically from a nib.
        print((scrollView?.frame.size.width)!)
        let count = singleJob.images.count
        scrollView?.contentSize = CGSize(width: (scrollWidth * CGFloat(count)), height: scrollHeight)
        scrollView?.delegate = self;
        scrollView?.isPagingEnabled=true
        
        for i in stride(from: 0, to: count, by: 1) {
            let imgView = UIImageView.init()
            imgView.frame = CGRect(x: scrollWidth * CGFloat (i), y: 0, width: scrollWidth,height: scrollHeight)
//            imgView.backgroundColor = UIColor.red
            ReciveData.loadImageFromInternetTableViewCell(urlString: singleJob.images[i], imageView: imgView)
            
//            if i == 0 {
////                imgView.backgroundColor = UIColor.green
//            }
//
//            if i == 1 {
////                imgView.backgroundColor = UIColor.blue
//                imgView.image = UIImage(named: "2.jpg")
//            }
            
            scrollView?.addSubview(imgView)
        }
        setJobInfoOnView()
        
        
        configViewElements()
        
//        downloadJobInfo()
        downloadJobAdds()
        downloadJobRevs(from: self.maxLoadedValue)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        authorDetailsView.isHidden = authorAddsIsHidden
        authorAddsViewHeightConstraint.constant = -authorDetailsView.frame.height
    }
    // MARK: - UITableViewDataSource protocol
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return jobReviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CommentTableViewCell
        
        let review = jobReviews[indexPath.section]
        cell.loadImageFromInternet(urlString: review.author_avatar)
        cell.userNameLabel.text = review.author
        cell.commentTextLabel.text = review.review
        if review.rec == "1" {
            cell.commentLikeImage.image = #imageLiteral(resourceName: "jobLike")
        } else {
            cell.commentLikeImage.image = #imageLiteral(resourceName: "jobUnlike")
        }
        cell.commentRatingCosmosView.rating = Double(review.rating)!
        let unixTimestamp = Int(review.date)
        let date = Date(timeIntervalSinceReferenceDate: TimeInterval(unixTimestamp!))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.full
        var convertedDate = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "dd MMM"
        dateFormatter.timeZone = NSTimeZone.local
        convertedDate = dateFormatter.string(from: date)
        cell.commentDateLabel.text = convertedDate
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.timeZone = NSTimeZone.local
        convertedDate = dateFormatter.string(from: date)
        cell.commentTimeLabel.text = convertedDate
        
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.height/2
        cell.commentBeckgroundView.layer.cornerRadius = 5
        cell.replyButton.layer.cornerRadius = 10
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.performSegue(withIdentifier: "ShowSubcategoriesVC", sender: categories[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let review = jobReviews[section]
        if review.answer == ""{
            return UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "footerCell") as! CommentTableViewFooterCell
        cell.authorImageView.image = authorImageView.image
        cell.authorReplyLabel.text = review.answer
        cell.authorReplyBackgroundView.layer.cornerRadius = 5
        cell.authorImageView.layer.cornerRadius = cell.authorImageView.frame.height/2
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == maxLoadedValue-1 {
            self.activityIndicator.startAnimating()
            self.activityIndicator.isHidden = false
            downloadJobRevs(from: maxLoadedValue)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let review = jobReviews[section]
        if review.answer == ""{
            return 0
        }
        return UITableViewAutomaticDimension
    }
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func showAddsInfoTapped(_ sender: Any) {
        if authorAddsIsHidden {
            authorDetailsView.isHidden = !authorAddsIsHidden
            authorAddsViewHeightConstraint.constant = 0
        } else {
            authorDetailsView.isHidden = !authorAddsIsHidden
            authorAddsViewHeightConstraint.constant = -authorDetailsView.frame.height
        }
        authorAddsIsHidden = !authorAddsIsHidden
    }
    @IBAction func likeTapped(_ sender: Any) {
        emojiView.isHidden = true
        emojiViewIsHidden = true
        if likeViewIsHidden {
            likeView.isHidden = !likeViewIsHidden
//            authorAddsViewHeightConstraint.constant = 0
        } else {
            likeView.isHidden = !likeViewIsHidden
//            authorAddsViewHeightConstraint.constant = -authorDetailsView.frame.height
        }
        likeViewIsHidden = !likeViewIsHidden

    }
    @IBAction func emojiTapped(_ sender: Any) {
        likeView.isHidden = true
        likeViewIsHidden = true
        if emojiViewIsHidden {
            emojiView.isHidden = !emojiViewIsHidden
        } else {
            emojiView.isHidden = !emojiViewIsHidden
        }
        emojiViewIsHidden = !emojiViewIsHidden

    }
    @IBAction func jobDescriptionTextTapped(_ sender: Any) {
        likeView.isHidden = true
        likeViewIsHidden = true
        emojiView.isHidden = true
        emojiViewIsHidden = true
        
        if descriptionTextIsHidden {
            infoAboutJobLabel.numberOfLines = 0
        } else {
            infoAboutJobLabel.numberOfLines = 5
        }
        descriptionTextIsHidden = !descriptionTextIsHidden
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        if segue.identifier == "ShowJobVC" {
        //            let newVC = segue.destination as! SubcategoriesViewCotroller
        //            newVC.parentCategory = sender as! Categories
        //        }  else if segue.identifier == "ShowSearchVC" {
        //            let newVC = segue.destination as! SearchViewCotroller
        //            newVC.searchText = sender as! String
        //        }
    }
    
    // MARK: - Additional functions
    func downloadJobInfo() -> Void {
        ReciveData.getData(dataType: ApiModel.single_job(job.id), completion: { data in
            if Int(data["res"]["code"].stringValue)! > 0 {
                self.defaults.set(data["job_price"].stringValue, forKey: DefaultsKeys.job_price)
                self.defaults.set(GlobalFunctions.setCurency(curency: data["job_currency"].stringValue) , forKey: DefaultsKeys.job_currency)
                let dictionary = data["job"].dictionaryValue

                self.singleJob.id = (dictionary["id"]?.stringValue)!
                self.singleJob.title = (dictionary["title"]?.stringValue)!
                self.singleJob.description = (dictionary["description"]?.stringValue)!
                self.singleJob.author_id = (dictionary["author_id"]?.stringValue)!
                self.singleJob.author = (dictionary["author"]?.stringValue)!
                self.singleJob.author_level = (dictionary["author_level"]?.stringValue)!
                self.singleJob.author_online = (dictionary["author_online"]?.stringValue)!
                self.singleJob.top = (dictionary["top"]?.stringValue)!
                self.singleJob.rating_count = (dictionary["rating_count"]?.stringValue)!
                self.singleJob.rating = (dictionary["rating"]?.stringValue)!
                self.singleJob.c1 = (dictionary["c1"]?.stringValue)!
                self.singleJob.c2 = (dictionary["c2"]?.stringValue)!
                self.singleJob.c3 = (dictionary["c3"]?.stringValue)!
                self.singleJob.c4 = (dictionary["c4"]?.stringValue)!
                self.singleJob.c5 = (dictionary["c5"]?.stringValue)!
                self.singleJob.c_show = (dictionary["c_show"]?.stringValue)!
                self.singleJob.rec_no = (dictionary["rec_no"]?.stringValue)!
                self.singleJob.rec_yes = (dictionary["rec_yes"]?.stringValue)!
                
                self.singleJob.author_avatar = (dictionary["author_avatar"]?.stringValue)!
                self.singleJob.is_fav = (dictionary["is_fav"]?.stringValue)!

                var images = (dictionary["images"]?.array)!
                let count = images.count
                for i in stride(from: 0, to: count, by: 1)  {
                    self.singleJob.images.append(images[i].stringValue)
                }
                self.jobFullInfo.singleJob = self.singleJob
                self.setJobInfoOnView()
                
            } else if Int(data["res"]["code"].stringValue)! < 0 {
                print (data["res"]["msg"].stringValue)
            }
//            self.downloadJobAdds()
        })
    }
    func downloadJobAdds() -> Void {
        ReciveData.getData(dataType: ApiModel.single_job_adds(singleJob.id), completion: { data in
            if Int(data["res"]["code"].stringValue)! > 0 {
                let dictionary = data["job"].dictionaryValue

                self.singleJobAdds.avg_order_done = (dictionary["avg_order_done"]?.stringValue)!
                self.singleJobAdds.verified_email = (dictionary["verified_email"]?.stringValue)!
                self.singleJobAdds.author_id = (dictionary["author_id"]?.stringValue)!
                self.singleJobAdds.author_info = (dictionary["author_info"]?.stringValue)!
                self.singleJobAdds.completed = (dictionary["completed"]?.stringValue)!
                self.singleJobAdds.queue = (dictionary["queue"]?.stringValue)!
                
                self.jobFullInfo.singleJobAdds = self.singleJobAdds
                self.setJobAddsOnView()
            } else if Int(data["res"]["code"].stringValue)! < 0 {
                print (data["res"]["msg"].stringValue)
            }
//            self.downloadJobRevs(from: self.maxLoadedValue)
        })
    }
    func downloadJobRevs(from: Int) -> Void {
        if maxExistedValue != 0 {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            return
        }
        ReciveData.getData(dataType: ApiModel.single_job_revs(singleJob.id, from, 5), completion: { data in
            if Int(data["res"]["code"].stringValue)! > 0 {
                let reviews = data["reviews"].arrayValue
                let count = reviews.count
                if count > 4 {
                    self.maxLoadedValue = from + 5
                } else {
                    self.maxLoadedValue  = from + count
                    self.maxExistedValue = self.maxLoadedValue
                }
                for i in stride(from: 0, to: count, by: 1)  {
                    self.jobReviews.append(JobReview(id: reviews[i]["id"].stringValue,
                                                     rating: reviews[i]["rating"].stringValue,
                                                     review: reviews[i]["review"].stringValue,
                                                     date: reviews[i]["date"].stringValue,
                                                     comm: reviews[i]["comm"].stringValue,
                                                     rec: reviews[i]["rec"].stringValue,
                                                     author_id: reviews[i]["author_id"].stringValue,
                                                     author: reviews[i]["author"].stringValue,
                                                     answer: reviews[i]["answer"].stringValue,
                                                     author_avatar: reviews[i]["author_avatar"].stringValue))
                }
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.jobFullInfo.jobReviews = self.jobReviews
                self.commentsTableView.reloadData()
            } else if Int(data["res"]["code"].stringValue)! < 0 {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                if self.jobReviews.count == 0 {
                    self.commentsTabeViewHeightConstraint.constant = 0
                }
                print (data["res"]["msg"].stringValue)
            }
            
        })
    }
    
    func setJobInfoOnView() -> Void {
        
        headerLabel.text = singleJob.title
        GlobalFunctions.setFavoriteStatusOnTop(object: favoriteButton, status: singleJob.is_fav)
//        ReciveData.loadImageFromInternetTableViewCell(urlString: singleJob.images[0], imageView: jobImageView)
        numberOfJobImageLabel.text = "1/\(singleJob.images.count)"
        ReciveData.loadImageFromInternetTableViewCell(urlString: singleJob.author_avatar, imageView: authorImageView)
        
        GlobalFunctions.setOnlineStatus(object: authorOnlineStatusImageView, status: singleJob.author_online)

        authorNameLabel.text = singleJob.author

        GlobalFunctions.setAuthorLevel(object: authorStatusImage, label: UILabel.init(), level: singleJob.author_level)
        jobHeaderLabel.text = singleJob.title
        
//        let font = UIFont(name: "Roboto-Condensed", size: 14)
//        let textAttribute = [NSAttributedStringKey.font: font as Any, NSAttributedStringKey.foregroundColor: UIColor.black]
        
        do {
//            let font = UIFont(name: "RobotoCondensed-Regular", size: 14)
//            let attributes: [NSAttributedStringKey: AnyObject] = [NSAttributedStringKey.font: font!]
            let attrStr = try NSAttributedString(
                data: singleJob.description.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
//            let length = attrStr.length
            
//            let textAttribute = [NSAttributedStringKey.font: font
//            attrStr.add
//            attrStr.addAttributes(attributes, range: NSRange(0..<length))
            infoAboutJobLabel.attributedText = attrStr
        } catch let error {
            print(error)
        }

        jobCommetsCountLabel.text = "Отзывы (\(Int(singleJob.rec_yes)! + Int(singleJob.rec_no)!))"
        ratingJobCosmosView.rating = Double(singleJob.rating)!
        ratingJobLabel.text = "(\(singleJob.rating))"
        
        countYesNoLabel()
        countEmoji()
    }
    
    func setJobAddsOnView() -> Void {
        aboutAuthorLabel.text = singleJobAdds.author_info
        authorDetailsView.layoutIfNeeded()
        authorAddsViewHeightConstraint.constant = -authorDetailsView.frame.height
        authorDetailsView.layoutIfNeeded()
        authorWorkTimeLabel.text = "Среднее время выполнения заказа - \(singleJobAdds.avg_order_done) дней"
        if singleJobAdds.verified_email == "1" {
            authorEmailApprovedLabel.text = "Электронная почта подтверждена"
        } else {
            authorEmailApprovedLabel.text = "Электронная почта не подтверждена"
        }
        
        authorOrdersDoneLabel.text = "Выполнено заказов - \(singleJobAdds.completed)"
        authorOrdersInQueueLabel.text = "Заказы на очереди - \(singleJobAdds.queue)"
    }
    
    func countYesNoLabel() -> Void {
        let yes = Double(singleJob.rec_yes)!
        let no = Double(singleJob.rec_no)!
        let total = yes+no
        var yesPercent = 0.0
        var noPercent = 0.0
        if total != 0.0 {
            yesPercent = yes/total*100
            noPercent = no/total*100
        }
        
        likeLabel.text = "\(Int(yesPercent))%"
        unlikeLabel.text = "\(Int(noPercent))%"
        if yesPercent >= noPercent {
            jobLikesImageView.image = likeImage.image
            jobLikesLabel.text = likeLabel.text
        } else {
            jobLikesImageView.image = unlikeImage.image
            jobLikesLabel.text = unlikeLabel.text
        }
    }
    func countEmoji() -> Void {
        let c1 = Double(singleJob.c1)!
        let c2 = Double(singleJob.c2)!
        let c3 = Double(singleJob.c3)!
        let c4 = Double(singleJob.c4)!
        let c5 = Double(singleJob.c5)!
        let total = c1+c2+c3+c4+c5
        
        var c1Percent = 0.0
        var c2Percent = 0.0
        var c3Percent = 0.0
        var c4Percent = 0.0
        var c5Percent = 0.0
        
        if total != 0.0 {
            c1Percent = c1/total*100
            c2Percent = c2/total*100
            c3Percent = c3/total*100
            c4Percent = c4/total*100
            c5Percent = c5/total*100
        }
        
        jobC1Label.text = "\(Int(c1Percent))%"
        jobC2Label.text = "\(Int(c2Percent))%"
        jobC3Label.text = "\(Int(c3Percent))%"
        jobC4Label.text = "\(Int(c4Percent))%"
        jobC5Label.text = "\(Int(c5Percent))%"
        let numbers = [c1Percent, c2Percent, c3Percent, c4Percent, c5Percent]
        if c1Percent == numbers.max() {
            jobEmojiLabel.text = jobC1Label.text
            jobEmojiImageView.image = jobC1Image.image
        } else if c2Percent == numbers.max() {
            jobEmojiLabel.text = jobC2Label.text
            jobEmojiImageView.image = jobC2Image.image
        } else if c3Percent == numbers.max() {
            jobEmojiLabel.text = jobC3Label.text
            jobEmojiImageView.image = jobC3Image.image
        } else if c4Percent == numbers.max() {
            jobEmojiLabel.text = jobC4Label.text
            jobEmojiImageView.image = jobC4Image.image
        } else if c5Percent == numbers.max() {
            jobEmojiLabel.text = jobC5Label.text
            jobEmojiImageView.image = jobC5Image.image
        }
        
    }

    func configViewElements() -> Void {
        authorImageView.layer.cornerRadius = authorImageView.frame.height/2
        authorStatusImage.layer.cornerRadius = authorStatusImage.frame.height/2
        
        openAuthorDetailsButton.layer.cornerRadius = openAuthorDetailsButton.frame.height/2
        openAuthorDetailsButton.layer.borderWidth = 1
        openAuthorDetailsButton.layer.borderColor = UIColor(hex: "EFEFEF").cgColor
        
        writeToAuthorButton.layer.cornerRadius = 5
        writeToAuthorButton.layer.borderWidth = 1
        writeToAuthorButton.layer.borderColor = UIColor(hex: "7ED321").cgColor
        
        orderJobButton.layer.cornerRadius = 5
        
        let tableHeight = UIScreen.main.bounds.height-orderJobView.frame.height-76
        commentsTabeViewHeightConstraint.constant = tableHeight
        
        likeView.layer.cornerRadius = 8
        emojiView.layer.cornerRadius = 8
        GlobalFunctions.setShadow(object: likeView)
        GlobalFunctions.setShadow(object: emojiView)
    }
}

//
//  SearchViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 10.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FilterResultsProtocol {
    @IBOutlet weak var jobsTableView: UITableView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var mySearchTextField: SearchTextField!
    @IBOutlet weak var searchCancelButton: UIButton!
    
    @IBOutlet weak var clearResultsView: UIView!
    
    let defaults = UserDefaults.standard
    
    var jobs = [Jobs]()
    var autocompleetResults = [SearchTextFieldItem]()
    
    var isSearch = false
    var searchText = ""
    var category = Subcategories()
    
    var maxLoadedValue = 0
    var maxExistedValue = 0
    
    var order = orderType.def
    var day_avg = day_avgType.def
    var online_sellers = onlineSellersType.def
    
    // MARK: - Main
    override func viewDidLoad() {
        if searchText != "" {
            headerLabel.text =  "Поиск по \""+searchText+"\""
        } else {
            headerLabel.text = category.name
        }
        downloadCategoryJobs(from: maxLoadedValue)
        initSearchTextView()
    }
    
    // MARK: - UITableViewDataSource protocol
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobs.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainTableViewCell
        cell.cellView.layer.cornerRadius = 5
        
        GlobalFunctions.setShadow(object: cell.cellView)
        //        cell.currentCellNumber = indexPath.row
        let job = jobs[indexPath.row]
        
        cell.serviceHeaderLabel.text = job.title
        cell.userNameLabel.text = job.author
        cell.servicePriceLabel.text = defaults.string(forKey: DefaultsKeys.job_price)!+"\u{20BD}"
        cell.userRatingCosmosView.rating = Double(job.rating)!
        cell.loadImageFromInternet(urlString: job.image)
        if job.rating_count == "0" {
            cell.userFeedbacksCountlabel.text = ""
            cell.userStatusName.text = ""
            cell.userStatusWidthConstraint.constant = 68
            cell.userStatusIcon.image = #imageLiteral(resourceName: "mainStatus")
        } else {
            cell.userFeedbacksCountlabel.text = "("+job.rating_count+")"
            GlobalFunctions.setAuthorLevel(object: cell.userStatusIcon, label: cell.userStatusName, level: job.author_level)
        }
        
        GlobalFunctions.setOnlineStatus(object: cell.onlineUserImageView, status: job.author_online)
        
        GlobalFunctions.setFavoriteStatus(object: cell.favoriteButton, status: job.fav)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == maxLoadedValue-1 {
            self.activityIndicator.startAnimating()
            self.activityIndicator.isHidden = false
            downloadCategoryJobs(from: maxLoadedValue)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.isUserInteractionEnabled = false
        downloadJobInfo(job: jobs[indexPath.row])
    }
    
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        mySearchTextField.isHidden = false
        
        mySearchTextField.becomeFirstResponder()
        mySearchTextField.filterItems(autocompleetResults)
        searchCancelButton.isHidden = false
    }
    
    @IBAction func searchCancelButtonTapped(_ sender: Any) {
        mySearchTextField.filterItems([SearchTextFieldItem]())
        mySearchTextField.text = ""
        mySearchTextField.resignFirstResponder()
        mySearchTextField.isHidden = true
        searchCancelButton.isHidden = true
    }
    @IBAction func filterButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "ShowFilterVC", sender: nil)
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowFilterVC" {
            let newVC = segue.destination as! FilterViewController
            newVC.order = order
            newVC.day_avg = day_avg
            newVC.online_sellers = online_sellers
            newVC.delegate      = self
        } else if segue.identifier == "ShowJobVC" {
            let newVC = segue.destination as! JobViewController
            newVC.singleJob = sender as! SingleJob
        }
    }
    
    // MARK: - Additional functions
    func downloadCategoryJobs(from: Int) -> Void {
        if maxExistedValue != 0 {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            return
        }
        ReciveData.getData(dataType: ApiModel.search(searchText, from, 10, category.id, order.rawValue, day_avg.rawValue, online_sellers.rawValue), completion: { data in
            if Int(data["res"]["code"].stringValue)! > 0 {
                let jobs = data["jobs"].arrayValue
                let count = jobs.count
                if count > 9 {
                    self.maxLoadedValue = from + 10
                } else {
                    self.maxLoadedValue  = from + count
                    self.maxExistedValue = self.maxLoadedValue
                }
                if count == 0 {
                    self.clearResultsView.isHidden = false
                }
                for i in stride(from: 0, to: count, by: 1)  {
                    self.jobs.append(Jobs(rating: jobs[i]["rating"].stringValue,
                                              author: jobs[i]["author"].stringValue,
                                              author_online: jobs[i]["author_online"].stringValue,
                                              id: jobs[i]["id"].stringValue,
                                              title: jobs[i]["title"].stringValue,
                                              image: jobs[i]["image"].stringValue,
                                              rating_count: jobs[i]["rating_count"].stringValue,
                                              top: jobs[i]["top"].stringValue,
                                              fav: jobs[i]["fav"].stringValue,
                                              author_level: jobs[i]["author_level"].stringValue))
                }
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.jobsTableView.reloadData()
            } else if Int(data["res"]["code"].stringValue)! < 0 {
                print (data["res"]["msg"].stringValue)
                GlobalFunctions.alertWithTitle(title: "Помилка", message: data["res"]["msg"].stringValue, ViewController: self)
            }
            
        })
    }
    func initSearchTextView() -> Void {
        mySearchTextField.setLeftPaddingPoints(20.0)
        mySearchTextField.theme.placeholderColor = UIColor.white
        mySearchTextField.userStoppedTypingHandler = {
            if let criteria = self.mySearchTextField.text {
                if criteria.characters.count > 1 {
                    self.autocompleetResults.removeAll()
                    GlobalFunctions.downloadAutocompleeteResults(search: criteria, completion: { (result) in
                        self.autocompleetResults = result
                        self.mySearchTextField.filterItems(self.autocompleetResults)
                    })
                }
            }
        }
        
        // Handle item selection - Default behaviour: item title set to the text field
        mySearchTextField.itemSelectionHandler = { filteredResults, itemPosition in
            if itemPosition == -1 {
                let title = self.mySearchTextField.text!
                self.searchCancelButtonTapped(self.searchCancelButton)
                self.startNewSearch(text: title)
                return
            }
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")
            
            // Do whatever you want with the picked item
            
            self.mySearchTextField.text = item.title
            self.searchCancelButtonTapped(self.searchCancelButton)
            self.startNewSearch(text: item.title)
        }
    }
    func startNewSearch(text: String) -> Void {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        jobs.removeAll()
        clearResultsView.isHidden = true
        searchText = text
        maxLoadedValue = 0
        maxExistedValue = 0
        order = orderType.def
        day_avg = day_avgType.def
        online_sellers = onlineSellersType.def
        category = Subcategories()
        if searchText != "" {
            headerLabel.text =  "Поиск по \""+searchText+"\""
        } else {
            headerLabel.text = category.name
        }
        downloadCategoryJobs(from: maxLoadedValue)
    }
    
    func filterResults(day_avgFilter: day_avgType, orderFilter: orderType, onlineSellersFilter: onlineSellersType) {
        order = orderFilter
        day_avg = day_avgFilter
        online_sellers = onlineSellersFilter
        jobs.removeAll()
        clearResultsView.isHidden = true
        maxLoadedValue = 0
        maxExistedValue = 0
        downloadCategoryJobs(from: maxLoadedValue)
    }
    
    func downloadJobInfo(job: Jobs) -> Void {
        ReciveData.getData(dataType: ApiModel.single_job(job.id), completion: { data in
            if Int(data["res"]["code"].stringValue)! > 0 {
                self.defaults.set(data["job_price"].stringValue, forKey: DefaultsKeys.job_price)
                self.defaults.set(GlobalFunctions.setCurency(curency: data["job_currency"].stringValue) , forKey: DefaultsKeys.job_currency)
                let dictionary = data["job"].dictionaryValue
                
                var singleJob = SingleJob()
                singleJob.id = (dictionary["id"]?.stringValue)!
                singleJob.title = (dictionary["title"]?.stringValue)!
                singleJob.description = (dictionary["description"]?.stringValue)!
                singleJob.author_id = (dictionary["author_id"]?.stringValue)!
                singleJob.author = (dictionary["author"]?.stringValue)!
                singleJob.author_level = (dictionary["author_level"]?.stringValue)!
                singleJob.author_online = (dictionary["author_online"]?.stringValue)!
                singleJob.top = (dictionary["top"]?.stringValue)!
                singleJob.rating_count = (dictionary["rating_count"]?.stringValue)!
                singleJob.rating = (dictionary["rating"]?.stringValue)!
                singleJob.c1 = (dictionary["c1"]?.stringValue)!
                singleJob.c2 = (dictionary["c2"]?.stringValue)!
                singleJob.c3 = (dictionary["c3"]?.stringValue)!
                singleJob.c4 = (dictionary["c4"]?.stringValue)!
                singleJob.c5 = (dictionary["c5"]?.stringValue)!
                singleJob.c_show = (dictionary["c_show"]?.stringValue)!
                singleJob.rec_no = (dictionary["rec_no"]?.stringValue)!
                singleJob.rec_yes = (dictionary["rec_yes"]?.stringValue)!
                
                singleJob.author_avatar = (dictionary["author_avatar"]?.stringValue)!
                singleJob.is_fav = (dictionary["is_fav"]?.stringValue)!
                
                var images = (dictionary["images"]?.array)!
                let count = images.count
                for i in stride(from: 0, to: count, by: 1)  {
                    singleJob.images.append(images[i].stringValue)
                }
                self.view.isUserInteractionEnabled = true
                self.performSegue(withIdentifier: "ShowJobVC", sender: singleJob)
                
            } else if Int(data["res"]["code"].stringValue)! < 0 {
                print (data["res"]["msg"].stringValue)
            }
        })
    }
    
}
protocol FilterResultsProtocol {
    func filterResults(day_avgFilter: day_avgType, orderFilter: orderType, onlineSellersFilter: onlineSellersType)
}

public enum day_avgType: String {
    case def     = ""
    case one     = "1"
    case three   = "3"
    case seven   = "7"
}

public enum orderType: String {
    case new = "new"
    case def = ""
    
}
public enum onlineSellersType: String {
    case online = "1"
    case def   = ""
    
}

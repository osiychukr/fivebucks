//
//  MainTableViewCell.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 05.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class MainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var onlineUserImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var serviceHeaderLabel: UILabel!
    @IBOutlet weak var servicePriceLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var userRatingCosmosView: CosmosView!
    @IBOutlet weak var userFeedbacksCountlabel: UILabel!
    @IBOutlet weak var userStatusIcon: UIImageView!
    @IBOutlet weak var userStatusName: UILabel!
    
    @IBOutlet weak var userStatusWidthConstraint: NSLayoutConstraint!
    
//    var currentCellNumber: Int!
    
    func loadImageFromInternet(urlString: String) -> Void { ReciveData.loadImageFromInternetTableViewCell(urlString: urlString, imageView: self.serviceImageView)
        
    }
}

//
//  ViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 05.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var mainTitleView: UIView!
    @IBOutlet weak var hiddenView: UIView!
    @IBOutlet weak var viewWithCategories: UIView!
    @IBOutlet weak var categoriesButton: UIButton!
    @IBOutlet weak var servicesTableView: UITableView!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var hiddenScrollView: UIScrollView!
    
    @IBOutlet var hideViewForScrolling: UIView!
    
    @IBOutlet weak var mySearchTextField: SearchTextField!
    @IBOutlet weak var searchCancelButton: UIButton!
    
    @IBOutlet weak var mainScrollHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var servicesTableViewHeightConstraint: NSLayoutConstraint!
    
    //menu
    @IBOutlet weak var backgroundMenuView: UIView!
    
    @IBOutlet weak var loginMenu: UIScrollView!

    @IBOutlet weak var userMenu: UIScrollView!
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userBalanceLabel: UILabel!
    
    
    
    let defaults = UserDefaults.standard
    
    var homeJobs = [Jobs]()
    var autocompleetResults = [SearchTextFieldItem]()
    
    // MARK: - Main
    override func viewDidLoad() {
        super.viewDidLoad()
        categoriesButton.layer.cornerRadius = 10
        GlobalFunctions.setShadow(object: categoriesButton)
        
        initSearchTextView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        recountHeightsForScrolls()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UITableViewDataSource protocol
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeJobs.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainTableViewCell
        cell.cellView.layer.cornerRadius = 5

        GlobalFunctions.setShadow(object: cell.cellView)
//        cell.currentCellNumber = indexPath.row
        let job = homeJobs[indexPath.row]
        
        cell.serviceHeaderLabel.text = job.title
        cell.userNameLabel.text = job.author
        cell.servicePriceLabel.text = defaults.string(forKey: DefaultsKeys.job_price)!+"\u{20BD}"
        cell.userRatingCosmosView.rating = Double(job.rating)!
        cell.loadImageFromInternet(urlString: job.image)
        if job.rating_count == "0" {
            cell.userFeedbacksCountlabel.text = ""
            cell.userStatusName.text = ""
            cell.userStatusWidthConstraint.constant = 68
            cell.userStatusIcon.image = #imageLiteral(resourceName: "mainStatus")
        } else {
            cell.userFeedbacksCountlabel.text = "("+job.rating_count+")"
            GlobalFunctions.setAuthorLevel(object: cell.userStatusIcon, label: cell.userStatusName, level: job.author_level)
        }
        
        GlobalFunctions.setOnlineStatus(object: cell.onlineUserImageView, status: job.author_online)
        
        GlobalFunctions.setFavoriteStatus(object: cell.favoriteButton, status: job.fav)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.isUserInteractionEnabled = false
        downloadJobInfo(job: homeJobs[indexPath.row])
    }
    
    // MARK: - Scroll View
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //stop text view
        if mainScrollView.convert(hideViewForScrolling.frame, to: self.view).minY <= mainTitleView.frame.maxY {
            viewWithCategories.layer.transform = CATransform3DMakeTranslation( 0, mainTitleView.frame.maxY-mainScrollView.convert(hideViewForScrolling.frame, to: self.view).minY, 0)
        } else if mainScrollView.convert(viewWithCategories.frame, to: self.view).minY > mainTitleView.frame.maxY {
            
            viewWithCategories.layer.transform = CATransform3DIdentity
        }

        
        
        // show image
        if mainScrollView.convert(hideViewForScrolling.frame, to: self.view).minY-mainTitleView.frame.maxY < hiddenScrollView.frame.height/2 && hiddenView.alpha == 1 {
            UIView.animate(withDuration: 0.7, delay: 0, options: [], animations: {
                self.hiddenView.alpha = 0
            })
        } else if mainScrollView.convert(hideViewForScrolling.frame, to: self.view).minY-mainTitleView.frame.maxY > hiddenScrollView.frame.height/2 && hiddenView.alpha != 1 {
            UIView.animate(withDuration: 0.7, delay: 0, options: [], animations: {
                self.hiddenView.alpha = 1
            })
        }

        //scroll image
        if scrollView.contentOffset.y > 0 {
            
            let foregroundHeight = mainScrollView.contentSize.height - mainScrollView.bounds.height
            let percentageScroll = mainScrollView.contentOffset.y / foregroundHeight
            let backgroundHeight = hiddenScrollView.contentSize.height
            
            hiddenScrollView.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, backgroundHeight * percentageScroll, 0)
        } else if scrollView.contentOffset.y <= 0 {
            UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: {
                self.hiddenScrollView.layer.transform = CATransform3DIdentity
            })
        }
        
    }
    // MARK: - Actions
    @IBAction func menuButtonTapped(_ sender: Any) {
        self.view.isUserInteractionEnabled = false
        showMenu(menu: loginMenu)
//        showMenu(menu: userMenu)
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        mySearchTextField.isHidden = false
        
        mySearchTextField.becomeFirstResponder()
        mySearchTextField.filterItems(autocompleetResults)
        searchCancelButton.isHidden = false
    }
    
    @IBAction func categoriesButtonTapped(_ sender: Any) {
        
        self.performSegue(withIdentifier: "ShowCategoriesVC", sender: nil)
    }
    @IBAction func searchCancelButtonTapped(_ sender: Any) {
        mySearchTextField.filterItems([SearchTextFieldItem]())
        mySearchTextField.text = ""
        mySearchTextField.resignFirstResponder()
        mySearchTextField.isHidden = true
        searchCancelButton.isHidden = true
    }
    
    // MARK: - MenuBurger Login Actions
    
    @IBAction func enterButtonTapped(_ sender: Any) {
        hideMenu(menu: loginMenu)
        self.performSegue(withIdentifier: "ShowLoginVC", sender: nil)
    }
    
    @IBAction func registrationButtonTappped(_ sender: Any) {
        hideMenu(menu: loginMenu)
        self.performSegue(withIdentifier: "ShowSignVC", sender: nil)
    }
    
    @IBAction func blogButtonTapped(_ sender: Any) {
        UIApplication.shared.open(URL(string: "https://blog.5bucks.ru/")!, options: [:])
    }
    
    @IBAction func rulesButtonTapped(_ sender: Any) {
        hideMenu(menu: loginMenu)
        self.performSegue(withIdentifier: "ShowRulesVC", sender: nil)
    }
    
    @IBAction func contactsButtonTapped(_ sender: Any) {
        hideMenu(menu: loginMenu)
        self.performSegue(withIdentifier: "ShowContactsVC", sender: nil)
    }
    // MARK: - MenuBurger User Actions
    
    @IBAction func messagesButtonTapped(_ sender: Any) {
    }
    @IBAction func notificationsButtonTapped(_ sender: Any) {
    }
    @IBAction func favoritesButtonTapped(_ sender: Any) {
    }
    @IBAction func myEarnsButtonTapped(_ sender: Any) {
    }
    @IBAction func mySalesButtonTapped(_ sender: Any) {
    }
    @IBAction func myJobsButtonTapped(_ sender: Any) {
    }
    @IBAction func moveUpJobButtonTapped(_ sender: Any) {
    }
    @IBAction func usersQueryButtonTapped(_ sender: Any) {
    }
    @IBAction func myBuysButtonTapped(_ sender: Any) {
    }
    @IBAction func myQueryButtonTapped(_ sender: Any) {
    }
    @IBAction func bucketButtonTapped(_ sender: Any) {
    }
    @IBAction func settingsButtonTapped(_ sender: Any) {
    }
    @IBAction func exitButtonTapped(_ sender: Any) {
    }
    
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowCategoriesVC" {
            _ = segue.destination as! CategoriesViewController
        } else if segue.identifier == "ShowSearchVC" {
            let newVC = segue.destination as! SearchViewController
            newVC.searchText = sender as! String
        } else if segue.identifier == "ShowJobVC" {
            let newVC = segue.destination as! JobViewController
            newVC.singleJob = sender as! SingleJob
        }
    }
    
    // MARK: - Additional functions
    func recountHeightsForScrolls() -> Void {
        let cellHeight:CGFloat = 108.0
        
        var customHeight = cellHeight * CGFloat(homeJobs.count)
        if customHeight < UIScreen.main.bounds.height-mainTitleView.frame.height-viewWithCategories.frame.height {
            customHeight = UIScreen.main.bounds.height-mainTitleView.frame.height-viewWithCategories.frame.height-20
        }
        self.servicesTableViewHeightConstraint.constant = customHeight
        self.servicesTableView.layoutIfNeeded()
    }
    
    func initSearchTextView() -> Void {
        mySearchTextField.setLeftPaddingPoints(20.0)
        mySearchTextField.theme.placeholderColor = UIColor.white
        mySearchTextField.userStoppedTypingHandler = {
            if let criteria = self.mySearchTextField.text {
                if criteria.characters.count > 1 {
                    self.autocompleetResults.removeAll()
                    GlobalFunctions.downloadAutocompleeteResults(search: criteria, completion: { (result) in
                        self.autocompleetResults = result
                        self.mySearchTextField.filterItems(self.autocompleetResults)
                    })
                }
            }
        }
        
        // Handle item selection - Default behaviour: item title set to the text field
        mySearchTextField.itemSelectionHandler = { filteredResults, itemPosition in
            if itemPosition == -1 {
                let title = self.mySearchTextField.text!
                self.searchCancelButtonTapped(self.searchCancelButton)
                self.performSegue(withIdentifier: "ShowSearchVC", sender: title)
                return
            }
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")
            
            // Do whatever you want with the picked item
            
            self.mySearchTextField.text = item.title
            self.searchCancelButtonTapped(self.searchCancelButton)
            self.performSegue(withIdentifier: "ShowSearchVC", sender: item.title)
        }
    }
    
    func downloadJobInfo(job: Jobs) -> Void {
        ReciveData.getData(dataType: ApiModel.single_job(job.id), completion: { data in
            if Int(data["res"]["code"].stringValue)! > 0 {
                self.defaults.set(data["job_price"].stringValue, forKey: DefaultsKeys.job_price)
                self.defaults.set(GlobalFunctions.setCurency(curency: data["job_currency"].stringValue) , forKey: DefaultsKeys.job_currency)
                let dictionary = data["job"].dictionaryValue
                
                var singleJob = SingleJob()
                singleJob.id = (dictionary["id"]?.stringValue)!
                singleJob.title = (dictionary["title"]?.stringValue)!
                singleJob.description = (dictionary["description"]?.stringValue)!
                singleJob.author_id = (dictionary["author_id"]?.stringValue)!
                singleJob.author = (dictionary["author"]?.stringValue)!
                singleJob.author_level = (dictionary["author_level"]?.stringValue)!
                singleJob.author_online = (dictionary["author_online"]?.stringValue)!
                singleJob.top = (dictionary["top"]?.stringValue)!
                singleJob.rating_count = (dictionary["rating_count"]?.stringValue)!
                singleJob.rating = (dictionary["rating"]?.stringValue)!
                singleJob.c1 = (dictionary["c1"]?.stringValue)!
                singleJob.c2 = (dictionary["c2"]?.stringValue)!
                singleJob.c3 = (dictionary["c3"]?.stringValue)!
                singleJob.c4 = (dictionary["c4"]?.stringValue)!
                singleJob.c5 = (dictionary["c5"]?.stringValue)!
                singleJob.c_show = (dictionary["c_show"]?.stringValue)!
                singleJob.rec_no = (dictionary["rec_no"]?.stringValue)!
                singleJob.rec_yes = (dictionary["rec_yes"]?.stringValue)!
                
                singleJob.author_avatar = (dictionary["author_avatar"]?.stringValue)!
                singleJob.is_fav = (dictionary["is_fav"]?.stringValue)!
                
                var images = (dictionary["images"]?.array)!
                let count = images.count
                for i in stride(from: 0, to: count, by: 1)  {
                    singleJob.images.append(images[i].stringValue)
                }
                self.view.isUserInteractionEnabled = true
                self.performSegue(withIdentifier: "ShowJobVC", sender: singleJob)
                
            } else if Int(data["res"]["code"].stringValue)! < 0 {
                print (data["res"]["msg"].stringValue)
            }
        })
    }
    func showMenu(menu: UIScrollView) -> Void {
        
        
        UIView.animate(withDuration: 0.33, delay: 0, options: [],
                       animations: {
                        let transform = CATransform3DTranslate(CATransform3DIdentity, menu.frame.width, 0, 0)
                        menu.layer.transform = transform
                        self.backgroundMenuView.isHidden = false
        },
                       completion: { finished in
                        self.view.isUserInteractionEnabled = true
        })
    }
    
    @IBAction func swipeLeft(_ sender: Any) {
        self.view.isUserInteractionEnabled = false
        hideMenu(menu: loginMenu)
    }
    func hideMenu(menu: UIScrollView) -> Void {
        
        UIView.animate(withDuration: 0.33, delay: 0, options: [],
                       animations: {
                        menu.layer.transform = CATransform3DIdentity
                        self.backgroundMenuView.isHidden = true
        },
                       completion: { finished in
                        self.view.isUserInteractionEnabled = true
        })
    }
}


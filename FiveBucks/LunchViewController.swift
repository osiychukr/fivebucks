//
//  LunchViewController.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 06.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SystemConfiguration
import TTGSnackbar

struct DefaultsKeys {
    static let  job_price = "job_price"
    static let  job_currency = "job_currency"
}

struct Jobs {
    var rating: String = ""
    var author: String = ""
    var author_online: String = ""
    var id: String = ""
    var title: String = ""
    var image: String = ""
    var rating_count: String = ""
    var top: String = ""
    var fav: String = ""
    var author_level: String = ""
}

class LunchViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    var homeJobs = [Jobs]()
    
    // MARK: - Main
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() {
            
            
         
            ReciveData.getData(dataType: ApiModel.get_home_jobs, completion: { data in
                if Int(data["res"]["code"].stringValue)! > 0 {
                    self.defaults.set(data["job_price"].stringValue, forKey: DefaultsKeys.job_price)
                    self.defaults.set(GlobalFunctions.setCurency(curency: data["job_currency"].stringValue) , forKey: DefaultsKeys.job_currency)
                    let jobs = data["jobs"].arrayValue
                    let count = jobs.count
                    for i in stride(from: 0, to: count, by: 1)  {
                        self.homeJobs.append(Jobs(rating: jobs[i]["rating"].stringValue,
                                                  author: jobs[i]["author"].stringValue,
                                                  author_online: jobs[i]["author_online"].stringValue,
                                                  id: jobs[i]["id"].stringValue,
                                                  title: jobs[i]["title"].stringValue,
                                                  image: jobs[i]["image"].stringValue,
                                                  rating_count: jobs[i]["rating_count"].stringValue,
                                                  top: jobs[i]["top"].stringValue,
                                                  fav: jobs[i]["fav"].stringValue,
                                                  author_level: jobs[i]["author_level"].stringValue))
                    }
                    self.performSegue(withIdentifier: "ShowMainVC", sender: self.homeJobs)
                } else if Int(data["res"]["code"].stringValue)! < 0 {
                    print (data["res"]["msg"].stringValue)
                }
                
            })
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !Reachability.isConnectedToNetwork() {
            let snackbar = TTGSnackbar(message: "Відсутнє підключення до інтернету!",
                                       duration: .forever,
                                       actionText: "Повторити!",
                                       actionBlock: { (snackbar) in
                                        if Reachability.isConnectedToNetwork() {
                                            snackbar.dismiss()
                                            self.viewDidLoad()
                                        } else {
                                            snackbar.dismiss()
                                            self.viewDidAppear(true)
                                        }
            })
            snackbar.show()
            
        } else {
            
            sleep(1)
        }
    }
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let mainVC = segue.destination as! MainViewController
        mainVC.homeJobs       = sender as! [Jobs]
    }
    
}
public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

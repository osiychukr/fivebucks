//
//  SubcategoriesTableViewCell.swift
//  FiveBucks
//
//  Created by Roma Osiychuk on 09.10.17.
//  Copyright © 2017 Roma Osiychuk. All rights reserved.
//

import Foundation
import UIKit

class SubcategoriesTableViewCell: UITableViewCell {
    @IBOutlet weak var subcategoryHeaderLabel: UILabel!
}
